#!/bin/bash

echo "Collecting stats for badges..."

commits=`git rev-list --count HEAD`
last_tag=`git rev-list --tags --max-count=1`
latest_release_tag=`git describe --tags $last_tag`
echo "{\"commits\":\"$commits\", \"release_tag\":\"$latest_release_tag\"}" > badges.json