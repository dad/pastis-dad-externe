/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020)

[dad@cines.fr]

This software is a computer program whose purpose is to provide
a web application to create, edit, import and export archive
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

import { ElementFinder, element, by, browser, promise } from 'protractor';
import { BreadCrumb } from './bread-crumb.po';
import { ButtonMenu } from './button-menu.po';
import { LeftPanel } from './left-panel.po';
import { MetadataTable } from './metadata-table.po';
import * as fs from 'fs';
import { MetadataPopup } from './metadata-popup.po';
import { Popup } from './popup';
import { MetadataContextMenu } from './metadata-menu.po';
import { AttributesPopup } from './attributes-popup.po';
import AsyncUtils from '../class/async-utils';

export class EditProfilePage {

  constructor() {}

  public get leftPanel(): LeftPanel {
    return new LeftPanel(element(by.css('mat-sidenav.pastis-side-nav')));
  }

  public get breadCrumb(): BreadCrumb {
    return new BreadCrumb(element(by.css('div.pastis-metadata-option-entete-1')));
  }

  public get buttonMenu(): ButtonMenu {
    return new ButtonMenu(element(by.css('div.pastis-metadata-option-entete-2')));
  }

  public get metadataTable(): MetadataTable {
    return new MetadataTable(element(by.css('div.pastis-table-container > table')));
  }

  public get currentOpenedPopup(): Popup {
    return new Popup(element(by.css('mat-dialog-container')));
  }

  public get metadataPopup(): MetadataPopup {
    return new MetadataPopup(element(by.css('mat-dialog-container')));
  }
  
  public async navigateTo(url:string) {
    await browser.get(url);
  }

  public async waitForSpinner() {
    browser.wait(function() {
      return element(by.css('body > app-root > app-home > ngx-ui-loader > div.ngx-overlay')).isDisplayed().then(function(result){return !result});
    }, 20000);
  }

  public deleteAlreadyDownloadedFiles() {
    var filename = browser.params.downloadsPath + 'pastis_profile.rng';
    if (fs.existsSync(filename))
    {
      // delete if there is any existing file with same name
      fs.unlinkSync(filename);
    } 
  }

  public async verifyFileDownload(): Promise<boolean> {
    var filename = browser.params.downloadsPath + 'pastis_profile.rng';
    return browser.wait(function() {
      return fs.existsSync(filename);
    }, 30000);
  }

  public get metadataContextMenu(): MetadataContextMenu {
    return new MetadataContextMenu(element(by.css('.cdk-overlay-container .mat-menu-panel')));
  }
  
  public get attributesPopup(): AttributesPopup {
    return new AttributesPopup(element(by.css('mat-dialog-container')));
  }
}