/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020)

[dad@cines.fr]

This software is a computer program whose purpose is to provide
a web application to create, edit, import and export archive
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/

import { Given, When, Then, Before, After, setDefaultTimeout } from 'cucumber';
import { EditProfilePage } from '../pages/edit-profile.po';
import { ListProfilePage } from '../pages/list-profile.po';
import * as chai from 'chai';
import chaiAsPromised = require('chai-as-promised');
import { browser } from 'protractor';
import { FileTreeNode } from '../pages/file-tree-node.po';
import { Metadata } from '../pages/metadata.po';
import AsyncUtils from '../class/async-utils';

const pastisEditProfilePage  = new EditProfilePage();
const pastisListProfilePage  = new ListProfilePage();

const expect = chai.expect;
const assert = chai.assert;

Before(() => {
  chai.use(chaiAsPromised);
  browser.manage().window().maximize();
});

// Timeout assez long, permettant de debug le code sans 'cramer' les promises qui attendent.
setDefaultTimeout(60 * 80085);


Given('The user goes to the homepage', async function () {
    await pastisEditProfilePage.navigateTo("");
    await pastisListProfilePage.waitForSpinner();
});

Then('The user clicks on {string} tab', async function (string) {
    await pastisEditProfilePage.leftPanel.FileTreeTab.getTabByName(string).click();
});


Then('The user clicks on the edit profile button of the profile name {string}', async function (profileName) {
    await (await pastisListProfilePage.listProFile.getProfileByname(profileName)).openEditProfil();
});

Then('The user should see the {string} tab', async function (tabName) {
    let title: string = '';
    switch(tabName){
        case 'ENTÊTE': title = 'Entête';break;
        case 'RÈGLES': title = 'Règles';break;
        case 'ARBORESCENCE': title = "Unités d'archives";break;
        case 'OBJETS': title = 'Objets';break;
    }
    expect(await pastisEditProfilePage.leftPanel.FileTreeTabBody.getTitle()==title).to.be.true;
});

When('The user clicks on the export button', async function () {
    pastisEditProfilePage.deleteAlreadyDownloadedFiles();
    await pastisEditProfilePage.buttonMenu.ExportProfileButton.click();
});

Then('The RNG file is downloaded', async function () {
    expect(await pastisEditProfilePage.verifyFileDownload()).to.be.true;
});

Then('The user navigates to {string}', async function (nodesName) {
    let node:FileTreeNode = pastisEditProfilePage.leftPanel.FileTreeTabBody.getFileTreeNodes();
    let names: string[] = nodesName.split(',')
    for(let i = 0; i<names.length; i++) {
        let nameccurence = names[i].split('-');
        let nameo:string = nameccurence[0];
        let occurence:number = (nameccurence.length == 2) ? Number(nameccurence[1]) : 1;
        node = await AsyncUtils.findSequential(await node.getChildren(), async (fileNode) => {
            if (await fileNode.getName()==nameo){
                occurence--;
            }
            return (occurence==0 && (await fileNode.getName()==nameo));
        });
        await node.container.click();
    }
});

Then('The user navigates to root of tree', async function () {
    await pastisEditProfilePage.leftPanel.FileTreeTabBody.getFileTreeNodes().container.click();
});

Then('The user writes {string} in the field {string} of the metadata {string}', async function (text, fieldName, metadataName) {
    let metadata: Metadata = await pastisEditProfilePage.metadataTable.metadataTableBody.getMetadataByName(metadataName);
    switch(fieldName){
        case 'valeurFixe': await metadata.valeurFixe.setValue(text);break;
        case 'cardinalite': await metadata.cardinalite.setValue(text);break;
        case 'commentaire': await metadata.commentaire.setValue(text);break;
    }
});

Then('The user clicks on Ajouter une métadonnée', async function () {
    await pastisEditProfilePage.metadataTable.addMetadataButton.click();
});

Then('The user clicks on Ajouter une UA', async function () {
    await pastisEditProfilePage.metadataTable.addUAButton.click();
});

Then('The user adds the metadata {string}', async function(metadataName){
    await (await AsyncUtils.find(await pastisEditProfilePage.metadataPopup.getMetadataSearch(), async (m)=>{return (await m.getName())==metadataName})).addButton.click();
});

Then('The user validates the popup', async function(){
    await pastisEditProfilePage.metadataPopup.validate();
});

Then('The user cancels the popup', async function(){
    await pastisEditProfilePage.metadataPopup.cancel();
});

Then('Verify that the list of nodes of {string} is {string}', async function(nodesName, values){
    let node:FileTreeNode = pastisEditProfilePage.leftPanel.FileTreeTabBody.getFileTreeNodes();
    if (nodesName !== 'root') {
        let names: string[] = nodesName.split(',');
        for(let i = 0; i<names.length; i++) {
            let nameccurence: string[] = names[i].split('-');
            let nameo:string = nameccurence[0];
            let occurence:number = (nameccurence.length == 2) ? Number(nameccurence[1]) : 1;
            node = await AsyncUtils.findSequential(await node.getChildren(), async (fileNode) => {
                if (await fileNode.getName()==nameo){
                    occurence--;
                }
                return (occurence==0 && (await fileNode.getName()==nameo));
            });

            // let childrens:FileTreeNode[] = await node.getChildren();
            // for (let j = 0;j<childrens.length; j++){
            //     if (await childrens[j].getName()==nameo){
            //         occurence--;
            //         if  (occurence==0 && (await childrens[j].getName()==nameo)){
            //             node = childrens[j];
            //             break;
            //         }
            //     }
            // }
        }
    }
    let nodesNames: string[] = await AsyncUtils.map(await node.getChildren(), async c=>{return await c.getName()});
    let valuesArray = [];
    if(values != '') {
        valuesArray = values.split(',').sort()
    }
    assert.deepEqual(nodesNames.sort(), valuesArray);
});

Then('Verify that the value of the field {string} of the metadata {string} is {string}', async function (field, metadataName, value) {
    let nodeValue:string = await (await pastisEditProfilePage.metadataTable.metadataTableBody.getMetadataByName(metadataName)).getfieldByName(field).getValue();
    expect(nodeValue).to.equals(value);
});

Then('Verify that the list of options of the field {string} of the metadata {string} is {string}', async function (field, metadataName, values) {
    let metadata = await pastisEditProfilePage.metadataTable.metadataTableBody.getMetadataByName(metadataName);
    let nodeValue: string[] = await metadata.getfieldByName(field).getOptions();
    let valuesArray = [];
    if(values != '') {
        valuesArray = values.split(',').sort()
    }
    assert.deepEqual(nodeValue.sort(), valuesArray);
});

Then('Open the menu of the metadata {string}', async function (metadataName){
    await (await pastisEditProfilePage.metadataTable.metadataTableBody.getMetadataByName(metadataName)).openContextMenu();
});

Then('Close the popup', async function (){
    await pastisEditProfilePage.currentOpenedPopup.close()
});

Then('Verify that the list of options in the context menu of the metadata {string} is {string}', async function (metadataName, values){
    let labels: string[] = await pastisEditProfilePage.metadataContextMenu.getMenuLabelList();
    let valuesArray = [];
    if(values != '') {
        valuesArray = values.split(',').sort()
    }
    assert.deepEqual(labels.sort(), valuesArray);
});

Then('The user clicks on the item {string} of the context menu', async function (item:string){
    await (await pastisEditProfilePage.metadataContextMenu.getMenuOptionByLabel(item)).click();
});

Then('Verify that the list of attributes is {string}', async function(attributes){
    let attributs: string[] = await AsyncUtils.map(await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributes(), async a =>{return await a.getName()});
    let attributesArray = [];
    if(attributes != '') {
        attributesArray = attributes.split(',').sort()
    }
    assert.deepEqual(attributs.sort(), attributesArray); 
});

Then('The user writes {string} in the field {string} of the attribute {string}', async function(text,fieldName,attributeName){
    let field = (await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    await field.container.sendKeys(text);
});

Then('Verify that the value of the field {string} of the attribute {string} is {string}', async function(fieldName,attributeName,text){
    let field =  (await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    assert.deepEqual(await field.getValue(),text);
});

Then('The user clicks on Tout sélectionner', async function(){
    await pastisEditProfilePage.attributesPopup.attributesTableHeader.selectAllCheckbox.click();
});

Then('The user selects the attributes {string}', async function(attributes:string){
    await AsyncUtils.forEach(attributes.split(','),async (attr:string) => {(await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributeByName(attr)).checkbox.click();});
});

Then('Verify that the selected attributes are {string}', async function (expectedAttributes){
    let attributselected: string[] = await AsyncUtils.map(await pastisEditProfilePage.attributesPopup.attributesTableBody.getSelectedAttributes(), async attr=>{return await attr.getName()});
    let atts = [];
    if (expectedAttributes != ''){
        atts = expectedAttributes.split(',');
    }
    expect(attributselected).to.deep.equal(atts);
});

Then("Verify that the attribute {string} cannot be deselected", async function (attribute){
    expect((await (await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributeByName(attribute)).checkbox.getAttribute('class'))).to.match(/mat-checkbox-disabled/);
});

Then('Verify that the list of options of the field {string} of the attribute {string} is {string}', async function(fieldName,attributeName,options){
    let field =  (await pastisEditProfilePage.attributesPopup.attributesTableBody.getAttributeByName(attributeName)).getfieldByName(fieldName);
    let optionsArray = [];
    if(options != '') {
        optionsArray = options.split(',').sort()
    }
    assert.deepEqual((await field.getOptions()).sort(), optionsArray);
});

Then("The user clicks on Ajouter une UA of {string}", async function (nodesName){
    let node:FileTreeNode = pastisEditProfilePage.leftPanel.FileTreeTabBody.getFileTreeNodes();
    if (nodesName !== 'root') {
        let names: string[] = nodesName.split(',')
        for(let i = 0; i<names.length; i++) {
            node = await AsyncUtils.find(await node.getChildren(), async c=>{return await c.getName()==names[i]});
        }
    }
    node.addUAButton.click();
});

Then('Export the RNG profile', async function(){
    pastisEditProfilePage.deleteAlreadyDownloadedFiles();
    await pastisEditProfilePage.buttonMenu.ExportProfileButton.click();
    //await pastisEditProfilePage.verifyFileDownload();
});


Then('BREAKPOINT', async function(){
    console.log('BREAKPOINT');
});

After(function (test) {
    // Todo catch these *** exceptions
    browser.manage().addCookie({ name: 'zaleniumTestPassed', value: test.result.status == 'failed' ? 'false' : 'true' });
})