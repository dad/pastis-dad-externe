/*
Copyright © CINES - Centre Informatique National pour l'Enseignement Supérieur (2020)

[dad@cines.fr]

This software is a computer program whose purpose is to provide
a web application to create, edit, import and export archive
profiles based on the french SEDA standard
(https://redirect.francearchives.fr/seda/).


This software is governed by the CeCILL-C  license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/


/*
Basic configuration to run your cucumber
feature files and step definitions with protractor.
**/
// global.downloadsPath = 'c:\\zaledownload\\';
//global.downloadsPath = '/tmp/zaledownload/';

exports.config = {
  params: {
    downloadsPath: 'c:\\zaledownload\\'
  },
  SELENIUM_PROMISE_MANAGER: false,
  /**
   * The timeout in milliseconds for each script run on the browser. This
   * should be longer than the maximum time your application needs to
   * stabilize between tasks.
   */
  allScriptsTimeout: 25000,
  defaultTimeoutInterval: 30000,

  /**
    * Test framework to use. This may be one of: jasmine, mocha or custom.
    * Default value is 'jasmine'
    *
    * When the framework is set to "custom" you'll need to additionally
    * set frameworkPath with the path relative to the config file or absolute:
    *
    *   framework: 'custom',
    *   frameworkPath: './frameworks/my_custom_jasmine.js',
    *
    * See github.com/angular/protractor/blob/master/lib/frameworks/README.md
    * to comply with the interface details of your custom implementation.
    *
    * Jasmine is fully supported as test and assertion frameworks.
    * Mocha has limited support. You will need to include your
    * own assertion framework (such as Chai) if working with Mocha.
    */
  framework: 'custom',  // set to "custom" instead of cucumber.
  frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

  specs: [
    //'./features/*.feature'     // Specs here are the cucumber feature files,
    //'./features/export-default-profile.feature',
    //'./features/navigation.feature',
    './features/configureprofile.feature',
    // './features/spec.feature'
    //'./features/list-profile.feature'
  ],
  // cucumber command line options
  cucumberOpts: {
    require: [
      './specs/*.ts',
    ],  // require step definition files before executing features
    tags: [],                      // <string[]> (expression) only execute the features or scenarios with tags matching the expression
    strict: true,                  // <boolean> fail if there are any undefined or pending steps
    'dry-run': false,              // <boolean> invoke formatters without executing steps
    compiler: [],                   // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
  },
  /**
  * A callback function called once protractor is ready and available, and
  * before the specs are executed. If multiple capabilities are being run,
  * this will run once per capability.
  *
  * You can specify a file containing code to run by setting onPrepare to
  * the filename string. onPrepare can optionally return a promise, which
  * Protractor will wait for before continuing execution. This can be used if
  * the preparation involves any asynchronous calls, e.g. interacting with
  * the browser. Otherwise Protractor cannot guarantee order of execution
  * and may start the tests before preparation finishes.
  *
  * At this point, global variable 'protractor' object will be set up, and
  * globals from the test framework will be available. For example, if you
  * are using Jasmine, you can add a reporter with:
  *
  *    jasmine.getEnv().addReporter(new jasmine.JUnitXmlReporter(
  *      'outputdir/', true, true));
  *
  * If you need access back to the current configuration object,
  * use a pattern like the following:
  *
  *    return browser.getProcessedConfig().then(function(config) {
  *      // config.capabilities is the CURRENT capability being run, if
  *      // you are using multiCapabilities.
  *      console.log('Executing capability', config.capabilities);
  *    });
  */
  onPrepare: function () {
    const { Given, Then, When, Before, After } = require('cucumber');
    global.Given = Given;
    global.When = When;
    global.Then = Then;
    global.Before = Before;
    global.After = After;
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
  }
};
