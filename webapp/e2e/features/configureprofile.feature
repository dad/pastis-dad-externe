Feature: Export profile
    Scenario: Add metadata to the archival profile and configure them
        Given The user goes to the homepage
        Then The user clicks on the edit profile button of the profile name "Profil_seda_pastis_mvp"
        #"ARBORESCENCE" tab
        Then The user clicks on "ARBORESCENCE" tab
        Then The user writes "1" in the field "cardinalite" of the metadata "ArchiveUnit"
        Then The user writes "AU de 1er niveau" in the field "commentaire" of the metadata "ArchiveUnit"
        Then The user navigates to "ArchiveUnit,Content"
        Then Verify that the list of options of the field "valeurFixe" of the metadata "DescriptionLevel" is ",Fonds,Subfonds,Class,Collection,Series,Subseries,RecordGrp,SubGrp,File,Item,OtherLevel"
        Then The user writes "File" in the field "valeurFixe" of the metadata "DescriptionLevel"
        Then Verify that the list of options of the field "cardinalite" of the metadata "DescriptionLevel" is "0-1,1"
        Then The user writes "1" in the field "cardinalite" of the metadata "DescriptionLevel"
        Then The user writes "1-N" in the field "cardinalite" of the metadata "Title"
        Then The user writes "Titre de l'AU de 1er niveau" in the field "commentaire" of the metadata "Title"
        Then Open the menu of the metadata "Title"
        Then Verify that the list of options in the context menu of the metadata "Title" is "Dupliquer,Attributs de métadonnée,Supprimer"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the selected attributes are ""
        Then The user selects the attributes "xml:lang"
        Then Verify that the selected attributes are "xml:lang"
        Then The user validates the popup
        Then Open the menu of the metadata "Title"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the selected attributes are "xml:lang"
        Then Close the popup
        Then The user navigates to "ArchiveUnit,Content"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Description"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description"
        Then The user writes "0-1" in the field "cardinalite" of the metadata "Description"
        Then Open the menu of the metadata "Description"
        Then Verify that the list of options in the context menu of the metadata "Description" is "Dupliquer,Attributs de métadonnée,Supprimer"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then The user writes "fra" in the field "valeurFixe" of the attribute "xml:lang"
        Then The user selects the attributes "xml:lang"
        Then The user validates the popup
        Then Open the menu of the metadata "Description"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the value of the field "valeurFixe" of the attribute "xml:lang" is "fra"
        Then Verify that the selected attributes are "xml:lang"
        Then Close the popup
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Tag"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description,Tag"
        Then Verify that the list of options of the field "cardinalite" of the metadata "Tag" is "0-1,0-N,1-N,1"
        Then The user writes "0-N" in the field "cardinalite" of the metadata "Tag"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Writer"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description,Tag,Writer"
        Then The user writes "1" in the field "cardinalite" of the metadata "Writer"
        Then The user navigates to "ArchiveUnit,Content"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "CreatedDate"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description,Tag,Writer,CreatedDate"
        Then The user writes "1" in the field "cardinalite" of the metadata "CreatedDate"
        Then The user writes "Date de création" in the field "commentaire" of the metadata "CreatedDate"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Type"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description,Type,Tag,Writer,CreatedDate"
        Then Open the menu of the metadata "Type"
        Then Verify that the list of options in the context menu of the metadata "Type" is "Dupliquer,Attributs de métadonnée,Supprimer"
        Then The user clicks on the item "Supprimer" of the context menu
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,Content" is "DescriptionLevel,Title,Description,Tag,Writer,CreatedDate"
        Then The user navigates to "ArchiveUnit"
        #
        Then The user clicks on Ajouter une UA of "ArchiveUnit"
        Then Verify that the list of nodes of "ArchiveUnit" is "Content,ArchiveUnit"
        Then The user writes "1-N" in the field "cardinalite" of the metadata "ArchiveUnit"
        Then The user writes "AU de 2nd niveau" in the field "commentaire" of the metadata "ArchiveUnit"
        Then The user navigates to "ArchiveUnit,ArchiveUnit,Content"
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit,Content" is ""
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DescriptionLevel"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit,Content" is "DescriptionLevel"
        Then Verify that the list of options of the field "valeurFixe" of the metadata "DescriptionLevel" is ",Fonds,Subfonds,Class,Collection,Series,Subseries,RecordGrp,SubGrp,File,Item,OtherLevel"
        Then The user writes "Item" in the field "valeurFixe" of the metadata "DescriptionLevel"
        Then Verify that the list of options of the field "cardinalite" of the metadata "DescriptionLevel" is "0-1,1"
        Then The user writes "1" in the field "cardinalite" of the metadata "DescriptionLevel"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Title"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit,Content" is "DescriptionLevel,Title"
        Then The user writes "1" in the field "cardinalite" of the metadata "Title"
        Then The user navigates to "ArchiveUnit,ArchiveUnit"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DataObjectReference"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit" is "Content,DataObjectReference"
        Then The user writes "1" in the field "cardinalite" of the metadata "DataObjectReference"
        Then The user navigates to "ArchiveUnit,ArchiveUnit,DataObjectReference"
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit,DataObjectReference" is ""
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DataObjectGroupReferenceId"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit,ArchiveUnit,DataObjectReference" is "DataObjectGroupReferenceId"
        Then The user writes "1" in the field "cardinalite" of the metadata "DataObjectGroupReferenceId"
        Then The user navigates to root of tree
        Then The user clicks on Ajouter une UA
        Then Verify that the list of nodes of "root" is "ArchiveUnit,ArchiveUnit"
        Then The user writes "0-1" in the field "cardinalite" of the metadata "ArchiveUnit-2"
        Then The user writes "Autre AU de 1er niveau" in the field "commentaire" of the metadata "ArchiveUnit-2"
        Then The user navigates to "ArchiveUnit-2,Content"
        Then Verify that the list of nodes of "ArchiveUnit-2,Content" is ""
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DescriptionLevel"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit-2,Content" is "DescriptionLevel"
        Then Verify that the list of options of the field "valeurFixe" of the metadata "DescriptionLevel" is ",Fonds,Subfonds,Class,Collection,Series,Subseries,RecordGrp,SubGrp,File,Item,OtherLevel"
        Then The user writes "Item" in the field "valeurFixe" of the metadata "DescriptionLevel"
        Then The user writes "1" in the field "cardinalite" of the metadata "DescriptionLevel"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Title"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit-2,Content" is "DescriptionLevel,Title"
        Then The user writes "1" in the field "cardinalite" of the metadata "Title"
        Then The user navigates to "ArchiveUnit-2"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DataObjectReference"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit-2" is "Content,DataObjectReference"
        Then The user writes "1" in the field "cardinalite" of the metadata "DataObjectReference"
        Then The user navigates to "ArchiveUnit-2,DataObjectReference"
        Then Verify that the list of nodes of "ArchiveUnit-2,DataObjectReference" is ""
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "DataObjectGroupReferenceId"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchiveUnit-2,DataObjectReference" is "DataObjectGroupReferenceId"
        Then The user writes "1" in the field "cardinalite" of the metadata "DataObjectGroupReferenceId"
        #"ENTÊTE" tab
        Then The user clicks on "ENTÊTE" tab
        Then The user navigates to "ArchivalAgency"
        Then The user writes "19341594000017" in the field "valeurFixe" of the metadata "Identifier"
        Then The user navigates to root of tree
        Then The user writes "Doit permettre d'identifier l'opérateur technique réalisant le versement de manière unique et non équivoque" in the field "commentaire" of the metadata "TransferringAgency"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Comment"
        Then The user validates the popup
        Then Verify that the list of nodes of "root" is "Comment,Date,MessageIdentifier,CodeListVersions,ArchivalAgency,TransferringAgency"
        Then The user writes "0-1" in the field "cardinalite" of the metadata "Comment"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "ArchivalAgreement"
        Then The user validates the popup
        Then Verify that the list of nodes of "root" is "Comment,Date,MessageIdentifier,ArchivalAgreement,CodeListVersions,ArchivalAgency,TransferringAgency"
        Then The user writes "IN-TNR-0" in the field "valeurFixe" of the metadata "ArchivalAgreement"
        Then The user writes "1" in the field "cardinalite" of the metadata "ArchivalAgreement"
        #"RÈGLES" tab
        Then The user clicks on "RÈGLES" tab
        Then The user writes "1" in the field "cardinalite" of the metadata "OriginatingAgencyIdentifier"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "ArchivalProfile"
        Then The user validates the popup
        Then Verify that the list of nodes of "root" is "ArchivalProfile,OriginatingAgencyIdentifier,SubmissionAgencyIdentifier"
        Then The user writes "1" in the field "cardinalite" of the metadata "ArchivalProfile"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "AppraisalRule"
        Then The user validates the popup
        Then Verify that the list of nodes of "root" is "ArchivalProfile,OriginatingAgencyIdentifier,SubmissionAgencyIdentifier,AppraisalRule"
        Then The user writes "1" in the field "cardinalite" of the metadata "AppraisalRule"
        Then The user navigates to "AppraisalRule"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "StartDate"
        Then The user validates the popup
        Then Verify that the list of nodes of "AppraisalRule" is "StartDate,FinalAction"
        Then The user writes "1" in the field "cardinalite" of the metadata "StartDate"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Rule"
        Then The user validates the popup
        Then The user writes "1" in the field "cardinalite" of the metadata "Rule"
        Then The user writes "APP-00001" in the field "valeurFixe" of the metadata "Rule"
        Then Verify that the list of options of the field "valeurFixe" of the metadata "FinalAction" is ",Keep,Destroy"
        Then The user writes "Keep" in the field "valeurFixe" of the metadata "FinalAction"
        #"OBJETS" tab
        Then The user clicks on "OBJETS" tab
        Then The user writes "1-N" in the field "cardinalite" of the metadata "DataObjectGroup"
        Then The user navigates to "DataObjectGroup"
        Then The user writes "1" in the field "cardinalite" of the metadata "BinaryDataObject"
        Then The user navigates to "DataObjectGroup,BinaryDataObject"
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject" is "DataObjectVersion,Uri,MessageDigest"
        Then The user writes "BinaryMaster_1" in the field "valeurFixe" of the metadata "DataObjectVersion"
        Then The user writes "1" in the field "cardinalite" of the metadata "DataObjectVersion"
        Then The user writes "1" in the field "cardinalite" of the metadata "Uri"
        Then Verify that the list of options of the field "cardinalite" of the metadata "MessageDigest" is "1"
        Then Open the menu of the metadata "MessageDigest"
        Then Verify that the list of options in the context menu of the metadata "MessageDigest" is "Dupliquer,Attributs de métadonnée"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the attribute "algorithm" cannot be deselected
        Then The user writes "SHA-512" in the field "valeurFixe" of the attribute "algorithm"
        Then The user validates the popup
        Then Open the menu of the metadata "MessageDigest"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the value of the field "valeurFixe" of the attribute "algorithm" is "SHA-512"
        Then Close the popup
        Then The user navigates to "DataObjectGroup,BinaryDataObject"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Size"
        Then The user validates the popup
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject" is "DataObjectVersion,Uri,MessageDigest,Size"
        Then The user writes "1" in the field "cardinalite" of the metadata "Size"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "FormatIdentification"
        Then The user validates the popup
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject" is "DataObjectVersion,Uri,MessageDigest,Size,FormatIdentification"
        Then The user writes "1" in the field "cardinalite" of the metadata "FormatIdentification"
        Then The user navigates to "DataObjectGroup,BinaryDataObject,FormatIdentification"
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject,FormatIdentification" is ""
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "FormatLitteral"
        Then The user adds the metadata "FormatId"
        Then The user validates the popup
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject,FormatIdentification" is "FormatLitteral,FormatId"
        Then The user writes "1" in the field "cardinalite" of the metadata "FormatLitteral"
        Then The user writes "1" in the field "cardinalite" of the metadata "FormatId"
        Then The user navigates to "DataObjectGroup,BinaryDataObject"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "FileInfo"
        Then The user validates the popup
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject" is "DataObjectVersion,Uri,MessageDigest,Size,FormatIdentification,FileInfo"
        Then The user writes "1" in the field "cardinalite" of the metadata "FileInfo"
        Then The user navigates to "DataObjectGroup,BinaryDataObject,FileInfo"
        Then Verify that the list of nodes of "DataObjectGroup,BinaryDataObject,FileInfo" is "Filename"
        Then Verify that the list of options of the field "cardinalite" of the metadata "Filename" is "1"
        Then The user writes "Nom du fichier" in the field "commentaire" of the metadata "Filename"
        Then Export the RNG profile