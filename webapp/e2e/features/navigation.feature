Feature: Navigate Profile
    Scenario: Open Pastis and Click stuff around..
        Given The user goes to the homepage
        Then The user clicks on the edit profile button of the profile name "Profil_seda_pastis_mvp34"
        Then The user clicks on "ARBORESCENCE" tab
        Then The user navigates to "ArchiveUnit,Content"
        Then The user writes "YAHOOO" in the field "valeurFixe" of the metadata "Title"
        Then The user writes "Fonds" in the field "valeurFixe" of the metadata "DescriptionLevel"
        Then The user writes "1" in the field "cardinalite" of the metadata "DescriptionLevel"
        Then Verify that the value of the field "valeurFixe" of the metadata "Title" is "YAHOOO"
        Then Verify that the value of the field "cardinalite" of the metadata "DescriptionLevel" is "1"
        Then Verify that the list of options of the field "valeurFixe" of the metadata "DescriptionLevel" is ",Fonds,Subfonds,Class,Collection,Series,Subseries,RecordGrp,SubGrp,File,Item,OtherLevel"
        Then The user clicks on "ENTÊTE" tab
        Then The user navigates to "ArchivalAgency,Identifier"
        Then The user writes "Super commentaire" in the field "commentaire" of the metadata "Identifier"
        Then The user navigates to "ArchivalAgency"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "OrganizationDescriptiveMetadata"
        Then The user validates the popup
        Then Verify that the list of nodes of "ArchivalAgency" is "Identifier,OrganizationDescriptiveMetadata"
        Then The user clicks on "OBJETS" tab
        Then The user navigates to "DataObjectGroup"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "PhysicalDataObject"
        Then The user validates the popup
        Then The user navigates to "DataObjectGroup,PhysicalDataObject"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "PhysicalDimensions"
        Then The user validates the popup
        Then The user navigates to "DataObjectGroup,PhysicalDataObject,PhysicalDimensions"
        Then The user clicks on Ajouter une métadonnée
        Then The user adds the metadata "Width"
        Then The user adds the metadata "Height"
        Then The user adds the metadata "Depth"
        Then The user validates the popup
        Then Verify that the list of nodes of "DataObjectGroup,PhysicalDataObject,PhysicalDimensions" is "Width,Height,Depth"
        Then Open the menu of the metadata "Width"
        Then Verify that the list of options in the context menu of the metadata "Width" is "Dupliquer,Attributs de métadonnée,Supprimer"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the list of options of the field "valeurFixe" of the attribute "unit" is ",micrometre,4H,millimetre,MMT,centimetre,CMT,metre,inch,INH,foot,FOT"
        Then The user writes "metre" in the field "valeurFixe" of the attribute "unit"
        Then The user writes "Yahoo" in the field "commentaire" of the attribute "unit"
        Then The user selects the attributes "unit"
        Then The user validates the popup
        Then Open the menu of the metadata "Width"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the list of attributes is "unit"
        Then Verify that the selected attributes are "unit"
        Then Verify that the value of the field "valeurFixe" of the attribute "unit" is "metre"
        Then Verify that the value of the field "commentaire" of the attribute "unit" is "Yahoo"
        Then Close the popup
        Then The user clicks on "ENTÊTE" tab
        Then Open the menu of the metadata "MessageIdentifier"
        Then Verify that the list of options in the context menu of the metadata "MessageIdentifier" is "Dupliquer,Attributs de métadonnée"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the list of attributes is "schemeID,schemeName,schemeAgencyID,schemeAgencyName,schemeVersionID,schemeDataURI,schemeURI"
        Then Verify that the selected attributes are ""
        Then The user selects the attributes "schemeID"
        Then The user writes "YAHOOO" in the field "valeurFixe" of the attribute "schemeID"
        Then Verify that the value of the field "valeurFixe" of the attribute "schemeID" is "YAHOOO"
        Then Verify that the selected attributes are "schemeID"
        Then The user clicks on Tout sélectionner
        Then Verify that the selected attributes are "schemeID,schemeName,schemeAgencyID,schemeAgencyName,schemeVersionID,schemeDataURI,schemeURI"
        Then The user validates the popup
        Then Open the menu of the metadata "MessageIdentifier"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the list of attributes is "schemeID,schemeName,schemeAgencyID,schemeAgencyName,schemeVersionID,schemeDataURI,schemeURI"
        Then Verify that the selected attributes are "schemeID,schemeName,schemeAgencyID,schemeAgencyName,schemeVersionID,schemeDataURI,schemeURI"
        Then The user clicks on Tout sélectionner
        Then The user selects the attributes "schemeName"
        Then Verify that the selected attributes are "schemeName"
        Then The user validates the popup
        Then Open the menu of the metadata "MessageIdentifier"
        Then The user clicks on the item "Attributs de métadonnée" of the context menu
        Then Verify that the selected attributes are "schemeName"
        Then Close the popup