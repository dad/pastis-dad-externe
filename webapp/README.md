# PASTIS - _Profil d'Archivage Simple pour le Traitement de l'Information en Seda_ ![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A//raw.githubusercontent.com/pimenta-cines/json-test/master/badges.json&query=latest['value']&colorB=blue&style=flat)

>                                    An WEB application to edit and export archive profiles based on SEDA


- [Features](#features)
- [Screenshots](#screenshots)
- [Installation](#1-installation)
- [Tests](#tests)
- [API Reference](#api-reference)
- [Contribute](#contribute)
- [Credits](#creadits)
- [License](#license)




## 1. Installation

- ~~For the REST API, PASTIS uses WildFly (version 10.x) as an application server.~~
- ~~On the front-end, PASTIS is based on Angular 8.x.~~


## License

[![License](https://img.shields.io/badge/License-CeCILL-blue)](https://cecill.info/index.en.html)

Copyright 2020 © <a href="http://www.cines.fr" target="_blank">CINES</a>.

PASTIS is governed by the CeCILL-C license under French law and abiding by the rules of distribution of free software. 
**[CeCILL license](https://cecill.info/index.en.html)**


