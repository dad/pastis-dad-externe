import { Injectable } from '@angular/core';
import { FileNode } from 'src/app/profile/edit-profile/classes/file-node';
import { NoticeProfile, ProfileResponse } from 'src/app/profile/edit-profile/classes/profile-response';
import { PUA } from 'src/app/profile/models/pua.model';

@Injectable({
  providedIn: 'root'
})
export class PuaService {

  constructor() { }

  puaToFileNode(profileResponse:any){
    let profile: any = JSON.parse(profileResponse.profile);
    if (profile.notice) {
      let puaProfile: FileNode  = JSON.parse(profile.notice.profile);
      return puaProfile;
    } else {
      return profile; 
    }

  }

  exportPUA(){
    
  }
}
