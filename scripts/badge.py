import json
import gitlab
import requests
import urllib.parse
from github import Github
import sys


class Badge:

    def __init__(self, project_name, project_config_name):
        self.PROJECT_NAME = project_name
        self.PROJECT_CONFIG_NAME = project_config_name

    # Returns a gitLab connection based on the project configuration name found in '.python-gitlab.cfg'
    @staticmethod
    def get_gitlab_connection(project_config_name):
        gl = gitlab.Gitlab.from_config(str(project_config_name), ['.python-gitlab.cfg'])
        return gl

    # Returns a github connection based on acess token passed by argument to the main function
    @staticmethod
    def push_to_github(repo_name, token, file_name, commit_message):
        g = Github(token)
        repo = g.get_user().get_repo(repo_name)
        try:
            with open(file_name) as input_file:
                file = input_file.read()
                contents = repo.get_contents('badges.json', ref="master")
                print("Push content to github: ", file)
                repo.update_file(contents.path, commit_message, file, contents.sha, branch="master");
        except IOError as error:
            print('Could not read file : {0}', error)

    # Returns the number of active runners based on a given connection
    @staticmethod
    def get_gitlab_active_runners(connection):
        runners = connection.runners.list(scope='active')
        return runners

    # Returns the a gitlab project object given the connection name and the gitlab project name
    @staticmethod
    def get_gitlab_project(connection, project_name):
        # 2. Retrieving general information about the project
        project_list = connection.projects.list(all=True)
        project_git = object
        for project in project_list:
            if project.name == project_name:
                project_git = project
        return project_git

    # Exports the collected data set to a json file, and push to
    # a github repository
    @staticmethod
    def export_json(data_set):
        with open('badges.json', 'w') as outfile:
            json.dump(data_set, outfile)

    # Generate the correct url and send it to shields.io API to create a badge
    # See dynamic badge generation option at : https://shields.io/
    @staticmethod
    def generate_create_badge_params(badge_name, badge_label, link, query, color):
        query_value = query + '[\'value\']'
        #badges_url_cines = urllib.parse.quote('https://git.cines.fr/api/v4/projects/91/repository/files/scripts%2Fbadges.json/raw?ref=master',
        #                                      safe='/')
        badges_url_github = urllib.parse.quote("https://raw.githubusercontent.com/pimenta-cines/json-test/master/badges.json", safe="/")
        return {'name': badge_name,
                'link_url': link,
                'image_url': 'https://img.shields.io/badge/dynamic/json.svg?label='
                             + badge_label + '&url='+badges_url_github+'&query=' + query_value + '&colorB=' + color + '&style=flat'}

    # Given a data set and a gitlab project object, the function will directly generate badges via gitlab API
    # If the badge doesn't exist, it will generate one. Otherwise, it will update the existing badge.
    def generate_badges(self, data_set, project_git):
        badges_names = [badge.name for badge in project_git.badges.list()]
        badges_id = [badge.id for badge in project_git.badges.list()]
        badge_dict = dict(zip(badges_names, badges_id))
        print("#########################BADGES#################################")
        for (name, value) in data_set.items():
            if name in badges_names:
                print("Badge '{0}' already exists. Updating its value to {1}".format(str(name), str(value['value'])))
                # Getting badge, updating image_url and saving
                badge = project_git.badges.get(badge_dict[str(name)])
                new_link = self.generate_create_badge_params(name, str(value['label']), str(value['link']),
                                                             name, str(value['color']))
                badge.image_url = new_link['image_url']
                badge.link_url = new_link['link_url']
                badge.save()
            elif name not in badges_names:
                print("Badge '{0}' do not exist yet. Creating one with value {1}".format(str(name), str(value['value'])))
                new_badge_params = self.generate_create_badge_params(name, str(value['label']), str(value['link']),
                                                                     name, value['color'])
                project_git.badges.create(new_badge_params)
        print("################################################################")


if __name__ == "__main__":
    # 0. Get github token
    github_token = str(sys.argv[1])

    # 1. Initializing connection and grab the project
    badge_obj = Badge(project_name='pastis-dad-externe', project_config_name='pastis')
    connection_gitlab = badge_obj.get_gitlab_connection(badge_obj.PROJECT_CONFIG_NAME)
    project_gitlab = badge_obj.get_gitlab_project(connection_gitlab, badge_obj.PROJECT_NAME)
    projects = connection_gitlab.projects.list()

    # 2. Retrieving info about the project
    # For tags only : Original code from python gitlab api not working to retrieve last tag :
    # Trying via api
    tags = requests.get(connection_gitlab.url + '/api/v4/projects/' + str(project_gitlab.id) + '/repository/tags',
                        headers={'Authorization': connection_gitlab.private_token})
    latest_version = str(tags.json()[0]['name'])
    latest_version = latest_version[1:]

    issues = project_gitlab.issues.list()
    commits = project_gitlab.commits.list(all=True)
    issues_opened = project_gitlab.issues.list(state='opened')
    percentage_issues_opened = "{0:.2}".format(str(len(issues_opened) / len(issues) * 100)) + "%"
    num_active_runners = len(badge_obj.get_gitlab_active_runners(connection_gitlab))

    # 3. Printing retrieved data
    print("Project name : " + str(project_gitlab.name))
    print("Project issues count : " + str(len(issues)))
    print("Project issues opened : " + str(len(issues_opened)))
    print("Project issues opened (%) : " + percentage_issues_opened)
    print("Project commits : " + str(len(commits)))
    print("Version : " + latest_version)
    print("Active runners : " + str(num_active_runners))

    # 4. Create a json data set and send to export
    # The format must be : label : {'name' : the badge value,
    #                               'label': the badge label,
    #                               'color': the badge color,
    #                               'link' : the badge click link}
    # where label cannot have special characters, including space
    data = {"commits": {'value': len(commits), 'label': 'commits', 'color': 'sucess',
                        'link': connection_gitlab.url+'/dad/'+project_gitlab.name+'/-/commits/master'},
            "issues": {'value': str(len(issues)), 'label': 'issues', 'color': 'red',
                       'link': connection_gitlab.url+'/dad/'+project_gitlab.name+'/-/issues'},
            "issues_opened": {'value': str(len(issues_opened)), 'label': 'issues opened', 'color': 'yellow',
                              'link': connection_gitlab.url+'/dad/'+project_gitlab.name+'/-/issues?scope=all&utf8=%E2%9C%93&state=opened'},
            "perc_issues_opened": {'value': str(percentage_issues_opened), 'label': 'issues opened 2', 'color': 'yellow',
                                   'link': connection_gitlab.url+'/dad/'+project_gitlab.name+'/-/issues?scope=all&utf8=%E2%9C%93&state=opened'},
            "latest": {'value': str(latest_version), 'label': 'version', 'color': 'blue',
                       'link': connection_gitlab.url+'/dad/'+project_gitlab.name+'/-/releases/v'+latest_version},
            "num_active_runners": {'value': str(num_active_runners), 'label': 'active runners', 'color': 'lightgreen',
                                   'link':connection_gitlab.url+'/dad/'+project_gitlab.name},
            }
    # 5. Exporting
    badge_obj.export_json(data)
    # 6. Generating badges
    badge_obj.generate_badges(data, project_gitlab)
    # 7. Pushing generated file to github
    badge_obj.push_to_github('json-test', github_token, 'badges.json', 'Update from gitlab CINES')
