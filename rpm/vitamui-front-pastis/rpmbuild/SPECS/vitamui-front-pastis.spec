%define debug_package %{nil}
%define version_num %( git describe --tags --abbrev=0 )
Name:           vitamui-front-pastis
Version:        %{version_num}
Release:        1%{?dist}
Summary:        An WEB application to edit and export archive profiles (front)

License:        CeCILL-C
URL:            https://dci-gitlab.cines.fr/dad/pastis-dad-externe
Source0:        https://dci-gitlab.cines.fr/dad/pastis-dad-externe/-/archive/%{version}/pastis-dad-externe-%{version}.tar.gz

BuildRequires:  rh-nodejs12
Requires:       nginx

%description
An WEB application to edit and export archive profiles (front)

%prep
%setup -n pastis-dad-externe-%{version}

%build
cd webapp
source /opt/rh/rh-nodejs12/enable
export NODE_OPTIONS="--max-old-space-size=1024"
npm install
npx -p @angular/cli@10.2.1 ng build --configuration=rpm

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p %{buildroot}/vitamui/html
cd webapp/dist
tar cvzf %{buildroot}/vitamui/html/pastis-front.tar.gz pastis-front

%files
/vitamui/html/pastis-front.tar.gz
%doc

%post
cd /vitamui/html
tar xvzf /vitamui/html/pastis-front.tar.gz > /dev/null 2>&1 

%postun
rm -rf /vitamui/html

%define changelog_date %( date "+%a %b %d %Y" )
%changelog
* %{changelog_date} Centre Informatique National de l'Enseignement Supérieur <svp@cines.fr> - %{version}-%{release}
- First vitamui-front-pastis package
