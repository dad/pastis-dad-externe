%define debug_package %{nil}
%define __jar_repack 0
%define version_num %( git describe --tags --abbrev=0 )
Name:           vitamui-ui-pastis
Version:        %{version_num}
Release:        1%{?dist}
Summary:        An WEB application to edit and export archive profiles (API)

License:        CeCILL-C
URL:            https://dci-gitlab.cines.fr/dad/pastis-dad-externe
Source0:        https://dci-gitlab.cines.fr/dad/pastis-dad-externe/-/archive/%{version}/pastis-dad-externe-%{version}.tar.gz

BuildRequires:  rh-maven35
Requires: java-11-openjdk-headless

%description
An WEB application to edit and export archive profiles (API)

%prep
%setup -n pastis-dad-externe-%{version}

%build
cd rest-api
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
source /opt/rh/rh-maven35/enable
mvn -v
mvn clean package spring-boot:repackage -Pprod 


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p %{buildroot}/vitamui/lib
install -m 644 rest-api/target/PastisApi.jar %{buildroot}/vitamui/lib/PastisApi.jar

%postun
rm -rf /vitamui/lib

%files
/vitamui/lib/PastisApi.jar
%doc

%define changelog_date %( date "+%a %b %d %Y" )
%changelog
* %{changelog_date} Centre Informatique National de l'Enseignement Supérieur <svp@cines.fr> - %{version}-%{release}
- First vitamui-ui-pastis package
