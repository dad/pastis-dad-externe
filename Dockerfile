FROM openjdk:11
ARG JAR_FILE=rest-api/target/PastisApi.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]


